-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2016 at 09:29 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `register`
--

-- --------------------------------------------------------

--
-- Table structure for table `td_register`
--

CREATE TABLE `td_register` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `roll` varchar(255) NOT NULL,
  `registation` varchar(255) NOT NULL,
  `batch` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_register`
--

INSERT INTO `td_register` (`id`, `name`, `roll`, `registation`, `batch`, `subject`, `gender`, `date`) VALUES
(21, 'alauddin', '26', '5214', '4th', 'cse', 'male', 'a:3:{i:0;s:1:"2";i:1;s:3:"jan";i:2;s:8:"selected";}'),
(22, 'alom', '66', '5456', 'five', 'eee', 'male', 'a:3:{i:0;s:1:"4";i:1;s:5:"april";i:2;s:4:"1993";}'),
(23, 'rony', '22', '45', '9th', 'sicence', 'male', 'a:3:{i:0;s:1:"3";i:1;s:3:"mar";i:2;s:4:"1992";}'),
(24, 'labonimon', '5', '54545', '4', 'sd', 'female', 'a:3:{i:0;s:1:"2";i:1;s:3:"feb";i:2;s:4:"1991";}'),
(25, 'hafijul', '01', '02', '03th', 'itihas', 'male', 'a:3:{i:0;s:1:"1";i:1;s:3:"jan";i:2;s:4:"1990";}'),
(26, 'bilkis', '55', '44', '22', 'sds', 'female', 'a:3:{i:0;s:1:"4";i:1;s:5:"april";i:2;s:4:"1993";}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `td_register`
--
ALTER TABLE `td_register`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `td_register`
--
ALTER TABLE `td_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
